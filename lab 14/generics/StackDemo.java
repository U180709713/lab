package generics;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Shape;
import shapes.Square;

public class StackDemo {
    public static void main(String[] args) {



        Stack<Shape> shapeStack = new StackImpl<>();

        shapeStack.push(new Circle(5));
        shapeStack.push(new Rectangle(5, 10));

        System.out.println(shapeStack.toList());
        Stack<Circle> circleStack = new StackImpl<>();

        circleStack.push(new Circle(3));
        circleStack.push(new Circle(10));

        System.out.println(circleStack.toList());

        shapeStack.addAll(circleStack);

        System.out.println(shapeStack.toList());

//
//
//
//        Stack<Integer> stack = new StackArrayImpl<>();
//
//        stack.push(1);
//        stack.push(5);
//        stack.push(7);
//        stack.push(8);
//
//        System.out.println(stack.toList());
//
//
//        Stack<Integer> stackA = new StackArrayImpl<>();
//
//        stackA.push(3);
//        stackA.push(9);
//        stackA.push(2);
//        stackA.push(4);
//
//        System.out.println(stackA.toList());
//
//        stack.addAll(stackA);
//        System.out.println(stack.toList());
//
//
//        int total = 0;
//        while (!stack.empty()){
//            total += stack.pop();
//        }
//        System.out.println("total: " + total);
//        System.out.println(stack.toList());
    }

    }
