public class Main {
    public static void main(String[] args) {
        Point topLeft = new Point(10,10);
        Rectangle rec1 = new Rectangle(5,6,topLeft);
        System.out.println("Rectange Area: " + rec1.area());
        System.out.println("Rectange Perimeter: " + rec1.perimeter());

        Point[] points = rec1.corners();

        for (int i = 0 ; i< points.length;i++){
            System.out.println("Corner " +  i +" "+ points[i].xCoord+ " " + points[i].yCoord);

        }
    }
}
