package drawing.version3;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Shape;
import shapes.Square;

public class Drawing {
	
	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	//private ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>();

	public double calculateTotalArea(){
		double totalArea = 0;

		for (Shape shape : shapes){
			totalArea += shape.area();

		}

		return totalArea;
	}
	
	public void addShape(Shape shape) {
		shapes.add(shape);
	}
	
	/*public void addRectangle(Rectangle r) {
		rectangles.add(r);
	}*/
}
