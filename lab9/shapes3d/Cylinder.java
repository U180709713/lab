package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {

    private double heigth;

    public Cylinder(int radius, double heigth) {
        super(radius);
        this.heigth = heigth;
    }

    public double volume(){
        return heigth*super.area();
    }


    public double area(){
        return 2 * super.area() + 2*Math.PI * radius * heigth ;
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "heigth=" + heigth + " area=" + area() +  " volume="+ volume()+
                '}';
    }
}
