package shapes2d;


public class Circle {
    protected int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public  double area(){
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +" area="+ area() +
                '}';
    }
}
